using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Threading.Tasks;
using System.IO;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;
using System.Text;
using System.Net.Http;
using System.Security.Cryptography;
using System.IO.Pipes;
using System.Security.Policy;
using System.Xml.Linq;
using System.Web.Routing;

namespace WebApplication1
{
    public partial class WebForm1 : System.Web.UI.Page
    {

        //You can get a valid token at https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes
        //Insert your JWT token INSERT_VALID_ACCESS_TOKEN (Caso esteja usando o endpoint do FW, o token deve começar com "Bearer ")
        static string INSERT_VALID_ACCESS_TOKEN = "Insira o Token JWT";

        //deve escolher se vai utilizar as url's do FW ou do HUB
        //static string urlInicializar = "https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/initialize";
        //static string urlFinalizar = "https://fw2.bry.com.br/api/cms-signature-service/v1/signatures/finalize";
        static string urlInicializar = "https://hub2.bry.com.br/fw/v2/cms/pkcs1/assinaturas/acoes/inicializar";
        static string urlFinalizar = "https://hub2.bry.com.br/fw/v2/cms/pkcs1/assinaturas/acoes/finalizar";

        protected void Page_Load(object sender, EventArgs e)
        {

        }

        protected void Inicializar_Click(object sender, EventArgs e)
        {
            try
            {
                //The BRy-extension populates the TextBoxDadosCertificate with the data of the selected certificate, here we get this data.
                string certificateData = this.TextBoxDadosCertificado.Text;

                if (INSERT_VALID_ACCESS_TOKEN == "")
                    throw new Exception("⚠ Please Insert a valid Access Token At Line 22");

                //We call the function to initialize by passing the certificate data
                string responseInitialize = Inicializar(certificateData).Result;

                RespostaInicializar responseInitializeObj = new RespostaInicializar();

                //This function returns the data in JSON Object as a String. We need to Deserialize to use this data as an object
                responseInitializeObj = Deserialize<RespostaInicializar>(responseInitialize);

                //If an error occurs in the Deserialization, we throw an exception
                if (responseInitializeObj.signedAttributes == null)
                    throw new Exception(responseInitialize + "\\n⚠ Please, check if you have credits available and entered a valid JWT token.");

                //We populate the TextBoxSaidaInitializar with the data returned from the FW-HUB
                this.TextBoxSaidaInicializar.Text = responseInitialize;


                //We prepared an object that will be serialized and used to populate the TextBoxEntradaExtensao
                EntradaExtensao inputExtension = new EntradaExtensao();

                inputExtension.algoritmoHash = responseInitializeObj.hashAlgorithm;
                inputExtension.formatoDadosEntrada = "Base64";
                inputExtension.formatoDadosSaida = "Base64";

                for (int i = 0; i < responseInitializeObj.signedAttributes.Count; ++i)
                {
                    var input = new AssinaturasExtensao();
                    input.hashes.Add(responseInitializeObj.signedAttributes[i].content);
                    inputExtension.assinaturas.Add(input);
                }

                this.TextBoxEntradaExtensao.Text = Serialize<EntradaExtensao>(inputExtension);
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);

            }

        }

        protected void Finalizar_Click(object sender, EventArgs e)
        {
            try
            {
                //We get the data returned from the fw-hub
                String responseInitialize = this.TextBoxSaidaInicializar.Text;

                string certificateData = this.TextBoxDadosCertificado.Text;

                RespostaInicializar responseInitializeObj = new RespostaInicializar();

                //This function returns the data in JSON Object as a String. We need to Deserialize to use this data as an object
                responseInitializeObj = Deserialize<RespostaInicializar>(responseInitialize);

                //Object generated from the BRy-extension's output
                SaidaExtensao outputExtension = Deserialize<SaidaExtensao>(this.TextBoxSaidaExtensao.Text);

                //We call the Finalize method extension output, the initialization return and the certificate data to finalize the signature
                string responseFinalize = Finalizar(outputExtension, responseInitializeObj, certificateData).Result;
                //this.TextBoxFinalizar.Text = Finalize(outputExtension, responseInitializeObj).Result;
                this.TextBoxFinalizar.Text = responseFinalize;
            }
            catch (Exception ex)
            {
                ScriptManager.RegisterStartupScript(this, this.GetType(), "alerts", "javascript:alert('" + ex.Message + "')", true);
            }
        }

        public async static Task<string> Inicializar(string certificateData)
        {

            var requestContent = new MultipartFormDataContent();
            
            requestContent.Add(new StringContent("123"), "nonce");
            requestContent.Add(new StringContent("TIMESTAMP"), "profile");
            requestContent.Add(new StringContent("false"), "attached");
            requestContent.Add(new StringContent("SHA256"), "hashAlgorithm");
            requestContent.Add(new StringContent("SIGNATURE"), "operationType");
            requestContent.Add(new StringContent(certificateData), "certificate");

            requestContent.Add(new StringContent("1111"), "originalDocuments[0][nonce]");
            requestContent.Add(new StringContent("473287F8298DBA7163A897908958F7C0EAE733E25D2E027992EA2EDC9BED2FA8"), "originalDocuments[0][hash]");
            //requestContent.Add(new StringContent(""), "originalDocuments[0][content]");
            //requestContent.Add(new StringContent(""), "originalDocuments[0][comment]");
            //requestContent.Add(new StringContent(""), "originalDocuments[0][filename]");
            //requestContent.Add(new StringContent(""), "originalDocuments[0][signature]");

            //Buscar o caminho da aplicação
            string path = AppDomain.CurrentDomain.BaseDirectory;

            HttpClient client = new HttpClient();
           
            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);

            //Envia os dados de inicialização da assinatura
            var response = await client.PostAsync(urlInicializar, requestContent).ConfigureAwait(false);
                                                    
            // O resultado do BRy Framework é um json
            string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return result;

        }

        public async static Task<string> Finalizar(SaidaExtensao outputExtension, RespostaInicializar responseInitializeObj, String certificateData)
        {

            var requestContent = new MultipartFormDataContent();

            //Percorremos os dados cifrados pela extensão e os adicionamos ao objeto de finalização
            for (int i = 0; i < outputExtension.assinaturas.Count; ++i)
            {

                requestContent.Add(new StringContent("123"), "nonce");
                requestContent.Add(new StringContent("ADRC"), "profile");
                requestContent.Add(new StringContent("false"), "attached");
                requestContent.Add(new StringContent("BASE64"), "returnType");
                requestContent.Add(new StringContent("SHA256"), "hashAlgorithm");
                requestContent.Add(new StringContent("SIGNATURE"), "operationType");
                requestContent.Add(new StringContent(certificateData), "certificate");

                var teste = outputExtension.assinaturas[i].hashes[0];

                requestContent.Add(new StringContent("1111"), "finalizations[" + i + "][nonce]");
                requestContent.Add(new StringContent("473287F8298DBA7163A897908958F7C0EAE733E25D2E027992EA2EDC9BED2FA8"), "finalizations[" + i + "][hash]");
                requestContent.Add(new StringContent(responseInitializeObj.signedAttributes[0].content), "finalizations[" + i + "][signedAttributes]");
                requestContent.Add(new StringContent(teste), "finalizations[" + i + "][signatureValue]");
                //requestContent.Add(new StringContent(""), "finalizations[" + i + "][document]");
                //requestContent.Add(new StringContent(""), "finalizations[" + i + "][signature]");
            }

            HttpClient client = new HttpClient();

            client.DefaultRequestHeaders.Add("Authorization", INSERT_VALID_ACCESS_TOKEN);

            var response = await client.PostAsync(urlFinalizar, requestContent).ConfigureAwait(false);

            string result = await response.Content.ReadAsStringAsync().ConfigureAwait(false);

            return result;
        }

        public static string Serialize<T>(T obj)
        {
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            MemoryStream ms = new MemoryStream();
            serializer.WriteObject(ms, obj);
            string retVal = Encoding.UTF8.GetString(ms.ToArray());
            return retVal;
        }

        public static T Deserialize<T>(string json)
        {
            T obj = Activator.CreateInstance<T>();
            MemoryStream ms = new MemoryStream(Encoding.Unicode.GetBytes(json));
            DataContractJsonSerializer serializer = new DataContractJsonSerializer(obj.GetType());
            obj = (T)serializer.ReadObject(ms);
            ms.Close();
            return obj;
        }

    }

    // Data Contracts, utilizados para Serializar e Desserializar JSON Objects

    [DataContract]
    public class DataObjInicializar
    {
        [DataMember]
        public string profile;

        [DataMember]
        public string hashAlgorithm;

        [DataMember]
        public string operationType;

        [DataMember]
        public string certificate;

        [DataMember]
        public string attached;

        [DataMember]
        public string nonce;

    }

    [DataContract]
    public class RespostaInicializar
    {
        [DataMember]
        public string nonce;

        [DataMember]
        public string hashAlgorithm;

        [DataMember]
        public List<AssinaturasInicializadasIntern> signedAttributes;

    }

    [DataContract]
    public class AssinaturasInicializadasIntern
    {

        [DataMember]
        public string content;

        [DataMember]
        public string nonce;

    }

    [DataContract]
    public class EntradaExtensao
    {
        [DataMember]
        public string algoritmoHash;

        [DataMember]
        public string formatoDadosEntrada;

        [DataMember]
        public string formatoDadosSaida;

        [DataMember]
        public List<AssinaturasExtensao> assinaturas;

        public EntradaExtensao()
        {
            this.assinaturas = new List<AssinaturasExtensao>();
        }
    }

    [DataContract]
    public class AssinaturasExtensao
    {

        [DataMember]
        public List<string> hashes;

        public AssinaturasExtensao()
        {
            this.hashes = new List<string>();
        }
    }

    [DataContract]
    public class SaidaExtensao
    {

        [DataMember]
        public List<AssinaturasExtensao> assinaturas;

        public SaidaExtensao()
        {
            this.assinaturas = new List<AssinaturasExtensao>();
        }
    }

    [DataContract]
    public class FinalizarDataInternal
    {
        [DataMember]
        public string cifrado;

        [DataMember]
        public string nonce;
    }

    public class FinalizarData
    {
        [DataMember]
        public string certificate;

        [DataMember]
        public string nonreturnTypece;

        [DataMember]
        public string attached;

        [DataMember]
        public string hashAlgorithm;

        [DataMember]
        public string profile;

        [DataMember]
        public string nonce;

        [DataMember]
        public string operationType;

        [DataMember]
        public string formatoDeDados;

    }

}
