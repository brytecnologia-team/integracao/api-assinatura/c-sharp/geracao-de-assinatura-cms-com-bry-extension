
# Geração de Assinaturas CMS via FW2 / HUB - com BRy Extension

Este é exemplo de integração dos serviços da API de assinatura com clientes baseados em tecnologia C# ASP.NET

Este exemplo apresenta os passos necessários para a geração de assinatura 
  - Passo 1: Carregamento do conteúdo do certificado digital pela BRy Extension.
  - Passo 2: Inicialização da assinatura e produção dos artefatos signedAttributes.
  - Passo 3: Cifragem dos dados inicializados pela BRy Extension.
  - Passo 4: Finalização da assinatura e obtenção do artefato assinado.

### Variáveis que devem ser configuradas

O exemplo por consumir a API de assinatura necessita ser configurado com token de acesso válido.

Este token de acesso pode ser obtido através da documentação disponibilizada em [Docs da API de Assinatura](https://api-assinatura.bry.com.br) ou através da conta de usuário em [BRy Cloud](https://cloud.bry.com.br/home/usuarios/autenticado/aplicacoes).

Caso ainda não seja cadastrado, [cadastre-se](https://www.bry.com.br/) para ter acesso a nossa plataforma de serviços.

Além disso, no processo de geração de assinatura é obrigatória a posse de um certificado digital que identifica o autor do artefato assinado.

Por esse motivo, é necessário possuir algum certificado instalado em seu repositório local.

**Observação**

Por se tratar de uma informação sensível do usuário, reforçamos que a informação inserida no exemplo é utilizada pontualmente para a produção da assinatura.

| Variável | Descrição | 
| ------ | ------ | 
| INSERT_VALID_ACCESS_TOKEN | Token de acesso (access_token), obtido no BRyCloud (https://api-assinatura.bry.com.br/api-assinatura-digital#autenticacao-apis). Para uso corporativo, o token deve ser da pessoa  jurídica e para uso pessoal, da pessoa física. | 

**Os endpoints possuem parâmetros não obrigatórios que podem ser diferentes. Desta forma, recomendamos que a documentação Swagger seja seguida de acordo com o endpoint ([Framework](https://fw2.bry.com.br/api/cms-signature-service/swagger-ui.html#/) e [HUB v2](https://hub2.bry.com.br/swagger-ui/index.html#/Assinador%20CMS/inicializarAssinaturaV2UsingPOST)).**


## Adquirir um certificado digital

É muito comum no início da integração não se conhecer os elementos mínimos necessários para consumo dos serviços.

Para assinar digitalmente um documento, é necessário, antes de tudo, possuir um certificado digital, que é a identidade eletrônica de uma pessoa ou empresa.

O certificado, na prática, consiste em um arquivo contendo os dados referentes à pessoa ou empresa, protegidos por criptografia altamente complexa e com prazo de validade pré-determinado.

Os elementos que protegem as informações do arquivo são duas chaves de criptografia, uma pública e a outra privada. Sendo estes elementos obrigatórios para a execução deste exemplo.

**Entendido isso, como faço para obter meu certificado digital?**

[Obtenha agora](https://certificado.bry.com.br/certificate-issue-selection) um Certificado Digital Corporativo de baixo custo para testes de integração.

Entenda mais sobre o [Certificado Corporativo](https://www.bry.com.br/blog/certificado-digital-corporativo/).  


### Uso

    - Passo 1: Execute o Build da aplicação utilizando o Microsoft Visual Studio.
    - Passo 2: Certifique-se de que possui a BRy Extension instalada em seu navegador.
    - Passo 3: Execute o exemplo.
